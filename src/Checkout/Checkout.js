import React, { useEffect, useState } from 'react';
import io from "socket.io-client";
import { useForm } from "react-hook-form";
import { Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import './Checkout.css'
import OrderAPI from '../API/OrderAPI';
import Paypal from './Paypal';
import { changeCount } from '../Redux/Action/ActionCount';
import NoteAPI from '../API/NoteAPI';
import Detail_OrderAPI from '../API/Detail_OrderAPI';
import CouponAPI from '../API/CouponAPI';

const socket = io('http://localhost:8000/', {
    transports: ['websocket'], jsonp: false
});
socket.connect();

Checkout.propTypes = {

};


function Checkout(props) {

    const [orderID, setOrderID] = useState('')

    const [carts, set_carts] = useState([])

    const [total_price, set_total_price] = useState(0)

    const [discount, set_discount] = useState(0)

    // state load_map
    const [load_map, set_load_map] = useState(true)

    // state load_order
    const [load_order_status, set_load_order_status] = useState(false)

    const [check_action, set_check_action] = useState(false)


    useEffect(() => {

        if (check_action) {

            set_carts(JSON.parse(localStorage.getItem('carts')))

            Sum_Price(JSON.parse(localStorage.getItem('carts')), 0)

            set_check_action(false)
        }

    }, [check_action])

    // Hàm này dùng để tính tổng tiền
    function Sum_Price(carts, sum_price) {

        carts.map(value => {
            return sum_price += Number(value.count) * Number(value.price_product)
        })

        const total = Number(sum_price)

        if (localStorage.getItem('coupon')) {
            // GET localStorage
            const coupon = JSON.parse(localStorage.getItem('coupon'))

            set_discount((total * parseInt(coupon.promotion)) / 100)

            const newTotal = total - ((total * parseInt(coupon.promotion)) / 100) + Number(price)

            localStorage.setItem("total_price", newTotal)

            set_total_price(newTotal)
        } else {

            localStorage.setItem("total_price", total + Number(price))

            set_total_price(total + Number(price))

        }

    }

    const [show_error, set_show_error] = useState(false)

    const [information, set_information] = useState({
        fullname: '',
        phone: '',
        address: '',
        email: ''
    })

    const onChangeFullname = (e) => {
        set_information({
            fullname: e.target.value,
            phone: information.phone,
            address: information.address,
            email: information.email
        })
    }
    const onChangePhone = (e) => {
        set_information({
            fullname: information.fullname,
            phone: e.target.value,
            address: information.address,
            email: information.email
        })
    }

    const onChangeAddress = (e) => {
        set_information({
            fullname: information.fullname,
            phone: information.phone,
            address: e.target.value,
            email: information.email
        })
    }
    const onChangeEmail = (e) => {
        set_information({
            fullname: information.fullname,
            phone: information.phone,
            address: information.address,
            email: e.target.value
        })
    }

    // Hàm này dùng để check validation cho paypal
    useEffect(() => {

        checkValidation()

    }, [information])

    // Kiểm tra Paypal
    function checkValidation() {
        if (information.fullname === '') {
            set_show_error(true)
        } else {
            if (information.phone === '') {
                set_show_error(true)
            } else {
                if (information.email === '') {

                    localStorage.setItem('information', JSON.stringify(information))

                    set_show_error(true)
                } else {
                    set_show_error(false)
                }
            }
        }
    }


    const { register, handleSubmit, errors } = useForm();

    const [redirect, set_redirect] = useState(false)

    const [paymentFailed, setPaymentFailed] = useState(false);


    const [load_order, set_load_order] = useState(false)

    const count_change = useSelector(state => state.Count.isLoad)

    const dispatch = useDispatch()

    // Hàm này dùng để thanh toán offline
    const handler_Checkout = async (data) => {
        try {
            set_load_order(true)

            if (localStorage.getItem("id_coupon")) {

                const responseUpdate = await CouponAPI.updateCoupon(localStorage.getItem("id_coupon"))
                console.log(responseUpdate)

            }

            // data Delivery
            const data_delivery = {
                // id_delivery:  Math.random.toString(),
                fullname: information.fullname,
                phone: information.phone,
            }

            // Xứ lý API Delivery
            const response_delivery = await NoteAPI.post_note(data_delivery)

            // data Order
            const data_order = {
                id_user: sessionStorage.getItem('id_user'),
                address: information.address,
                total: total_price,
                status: "1",
                pay: false,
                id_payment: '6086709cdc52ab1ae999e882',
                id_note: response_delivery._id,
                feeship: price,
                id_coupon: localStorage.getItem('id_coupon') ? localStorage.getItem('id_coupon') : '',
                create_time: `${new Date().getDate()}/${parseInt(new Date().getMonth()) + 1}/${new Date().getFullYear()}`
            }

            // Xứ lý API Order
            const response_order = await OrderAPI.post_order(data_order)

            // data carts
            const data_carts = JSON.parse(localStorage.getItem('carts'))

            // Xử lý API Detail_Order
            for (let i = 0; i < data_carts.length; i++) {

                const data_detail_order = {
                    id_order: response_order._id,
                    id_product: data_carts[i].id_product,
                    name_product: data_carts[i].name_product,
                    price_product: data_carts[i].price_product,
                    count: data_carts[i].count,
                    size: data_carts[i].size
                }

                await Detail_OrderAPI.post_detail_order(data_detail_order)

            }

            const data_email = {
                id_order: response_order._id,
                total: total_price,
                fullname: information.fullname,
                phone: information.phone,
                price: price,
                address: information.address,
                email: information.email
            }

            // Gửi socket lên server
            socket.emit('send_order', "Có người vừa đặt hàng")
            // Xử lý API Send Mail

            const send_mail = await OrderAPI.post_email(data_email)
            console.log(send_mail)

            localStorage.removeItem('information')
            localStorage.removeItem('total_price')
            localStorage.removeItem('price')
            localStorage.removeItem('id_coupon')
            localStorage.removeItem('coupon')
            localStorage.setItem('carts', JSON.stringify([]))

            set_redirect(true)

            // Hàm này dùng để load lại phần header bằng Redux
            const action_count_change = changeCount(count_change)
            dispatch(action_count_change)
        } catch (error) {
            setPaymentFailed(true)
        }
    }

    const Change_Load_Order = () => {

        set_load_order(true)

    }


    //--------------- Xử lý Oenstreetmap API ------------------//

    const [error_address, set_error_address] = useState(false)

    const [from, set_from] = useState('2 Trường Sa, Phường 17, Quận Bình Thạnh, Thành Phố Hồ Chí Minh, Việt Nam')

    // Khoảng cách
    const [distance, set_distance] = useState('')

    // Thời gian đi trong bn phút
    /* const [duration, set_duration] = useState('') */

    // Giá tiền
    const [price, set_price] = useState('')


    // Kiểm tra xem khách hàng đã nhập chỉ nhận hàng hay chưa
    const handler_Next = () => {

        if (!information.address) {
            set_error_address(true)
            return
        }

        // Sau khi mà đổ dữ liệu ở bên Jquery xong
        // thì qua bên này mình sẽ lấy những giá trị vừa xử lý

        const kilo = document.getElementById('in_kilo').innerHTML
        const price_shipping = document.getElementById('price_shipping').innerHTML

        const to_places = document.getElementById('to_places').value

        console.log(kilo)
        console.log(price_shipping)

        set_distance(kilo)

        localStorage.setItem('price', price_shipping)
        set_price(price_shipping)

        set_information({
            fullname: information.fullname,
            phone: information.phone,
            address: to_places,
            email: information.email
        })
        if (kilo) {
            set_load_map(false)
            set_load_order_status(true) // Hiển thị phần checkout
            set_check_action(true)
        }
    }

    return (
        <div>
            {
                load_order && (
                    <div className="wrapper_loader">
                        <div className="loader"></div>
                    </div>
                )
            }
            <div className="breadcrumb-area">
                <div className="container">
                    <div className="breadcrumb-content">
                        <ul>
                            <li><a href="index.html">Trang chủ</a></li>
                            <li className="active">Thanh toán</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div className="container" style={{ paddingTop: '3rem', paddingBottom: '3rem' }}>
                {
                    load_map && (
                        <div className="row">
                            <div className="col-lg-6 col-12 pb-5">
                                <div className="checkbox-form">
                                    <h3 style={{ color: '#333' }}>Kiểm tra khoảng cách</h3>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="checkout-form-list">
                                                <label>Từ <span className="required"></span></label>
                                                <input type="text" name="from"
                                                    id="from_places"
                                                    disabled="true"
                                                    value={from}
                                                />
                                                <span style={{ color: '#888888', fontStyle: 'italic', fontSize: '12px', marginLeft: '3px' }}>
                                                    Địa chỉ cửa hàng
                                                </span>
                                                <input id="origin" name="origin" required="" type="hidden"
                                                    value={from} />
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="checkout-form-list">
                                                <label>Đến <span className="required">*</span></label>
                                                <input type="text"
                                                    id="to_places"
                                                    placeholder="Nhập địa chỉ nơi nhận"
                                                    value={information.address}
                                                    onChange={onChangeAddress} />
                                                {error_address && <span style={{ color: 'red' }}>* Địa chỉ bắt buộc</span>}
                                                <input id="destination" name="destination" required="" type="hidden" />
                                            </div>
                                        </div>
                                        {/* <div className="col-md-12">
                                            <div className="checkout-form-list">
                                                <div className="form-group hidden" style={{ display: 'none' }}>
                                                    <label>
                                                        Hình thức vận chuyển
                                                    </label>
                                                    <select id="travel_mode" name="travel_mode">
                                                        <option value="DRIVING">
                                                            Lái xe (DRIVING)
                                                        </option>
                                                         <option value="WALKING">
                                                            Đi bộ (WALKING)
                                                        </option>
                                                        <option value="BICYCLING">
                                                            Đi xe đạp (BICYCLING)
                                                        </option>
                                                        <option value="TRANSIT">
                                                            Giao thông công cộng (TRANSIT)
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div> */}
                                        <div className="col-md-12">
                                            <div id="result" className="hide">
                                                <div>
                                                    <label htmlFor="Kilometers">Khoảng cách: </label>&nbsp;
                                                    <label id="in_kilo"></label>
                                                </div>
                                                <div>
                                                    <label htmlFor="Price">Phí vận chuyển: </label>&nbsp;
                                                    <label id="price_shipping"></label>
                                                    &nbsp;<label>VNĐ</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="order-button-payment">
                                                <input value="Kiểm tra" type="submit" id="distance_form" />
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="d-flex justify-content-end">
                                                <div className="order-button-payment">
                                                    <input value="Tiếp theo" onClick={handler_Next} id="distance_next" type="submit" style={{ padding: '.4rem 1.6rem' }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-12">
                                {/* Load map */}
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.2422879034466!2d106.70274917480518!3d10.792746189357!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317528b47399cafb%3A0xfb5aa964e6e55268!2zMiDEkC4gVHLGsOG7nW5nIFNhLCBQaMaw4budbmcgMTcsIELDrG5oIFRo4bqhbmgsIFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1704988320670!5m2!1svi!2s" width="600" height="600" style={{ border: '0' }} loading="lazy"></iframe>
                            </div>
                        </div>
                    )
                }
                {
                    load_order_status && (
                        <div className="row">
                            <div className="col-lg-6 col-12 pb-5">
                                <form onSubmit={handleSubmit(handler_Checkout)}>
                                    <div className="checkbox-form">
                                        <h3>Chi tiết hóa dơn</h3>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="checkout-form-list">
                                                    <label>Tên <span className="required">*</span></label>
                                                    <input placeholder="Nhập tên" type="text" name="fullname"
                                                        ref={register({ required: true })}
                                                        value={information.fullname}
                                                        onChange={onChangeFullname} />
                                                    {errors.fullname && errors.fullname.type === "required" && <span style={{ color: 'red' }}>* Tên là bắt buộc</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12">
                                                <div className="checkout-form-list">
                                                    <label>Số điện thoại <span className="required">*</span></label>
                                                    <input placeholder="Nhập số điện thoại" type="text" name="phone"
                                                        ref={register({ required: true })}
                                                        value={information.phone}
                                                        onChange={onChangePhone} />
                                                    {errors.phone && errors.phone.type === "required" && <span style={{ color: 'red' }}>* SDT là bắt buộc</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12">
                                                <div className="checkout-form-list">
                                                    <label>Địa chỉ <span className="required"></span></label>
                                                    <input placeholder="Street address" type="text" name="address"
                                                        ref={register({ required: true })}
                                                        value={information.address}
                                                        onChange={onChangeAddress}
                                                        disabled="true" />
                                                    {errors.address && errors.address.type === "required" && <span style={{ color: 'red' }}>* Địa chỉ là bắt buộc</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12">
                                                <div className="checkout-form-list">
                                                    <label>Email <span className="required">*</span></label>
                                                    <input placeholder="Nhập email" type="email" name="email"
                                                        ref={register({ required: true })}
                                                        value={information.email}
                                                        onChange={onChangeEmail} />
                                                    {errors.email && errors.email.type === "required" && <span style={{ color: 'red' }}>* Email là bắt buộc</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12">
                                                <div className="order-button-payment">
                                                    {
                                                        redirect && <Redirect to="/success" />
                                                    }

                                                    {
                                                        paymentFailed && <Redirect to="/fail" />

                                                    }
                                                    <input value="Thanh toán khi nhận hàng" type="submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="col-lg-6 col-12">
                                <div className="your-order">
                                    <h3>Đơn hàng của bạn</h3>
                                    <div className="your-order-table table-responsive">
                                        <table className="table">
                                            <thead>
                                                <tr>
                                                    <th className="cart-product-name">Sản phẩm</th>
                                                    <th className="cart-product-total">Tổng</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    carts && carts.map(value => (
                                                        <tr className="cart_item" key={value._id}>
                                                            <td className="cart-product-name">{value.name_product}<strong className="product-quantity"> × {value.count}</strong></td>
                                                            <td className="cart-product-total"><span className="amount">{new Intl.NumberFormat('vi-VN', { style: 'decimal', decimal: 'VND' }).format(parseInt(value.price_product) * parseInt(value.count)) + ' VNĐ'}</span></td>
                                                        </tr>
                                                    ))
                                                }
                                            </tbody>
                                            <tfoot>
                                                <tr className="cart-subtotal">
                                                    <th>Giá ship</th>
                                                    <td><span className="amount">{new Intl.NumberFormat('vi-VN', { style: 'decimal', decimal: 'VND' }).format(price) + ' VNĐ'}</span></td>
                                                </tr>
                                                <tr className="cart-subtotal">
                                                    <th>Giảm giá</th>
                                                    <td><span className="amount">{new Intl.NumberFormat('vi-VN', { style: 'decimal', decimal: 'VND' }).format(discount) + ' VNĐ'}</span></td>
                                                </tr>
                                                <tr className="order-total">
                                                    <th>Tổng đơn hàng</th>
                                                    <td><strong><span className="amount">{new Intl.NumberFormat('vi-VN', { style: 'decimal', decimal: 'VND' }).format(total_price) + ' VNĐ'}</span></strong></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div className="payment-method">
                                        <div className="payment-accordion">
                                            <div id="accordion">
                                                <div className="card">
                                                    <div className="card-header" id="#payment-3">
                                                        <h5 className="panel-title">
                                                            <p style={{ color: 'white' }}>Thanh toán qua ví điện tử</p>
                                                            <a className="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                PayPal
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseThree" className="collapse">
                                                        <div className="panel-title">
                                                            {
                                                                show_error ? 'Vui lòng điền thông tin trước!' :
                                                                    <Paypal
                                                                        information={information}
                                                                        total={total_price}
                                                                        Change_Load_Order={Change_Load_Order}
                                                                        from={from}
                                                                        distance={distance}
                                                                        /* duration={duration} */
                                                                        price={price}
                                                                    />
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {/*   <div>
                                                <div className="card">
                                                    <div className="card-header" id="#payment-3">
                                                        <h5 className="panel-title">
                                                            <a className="collapsed" data-toggle="collapse" data-target="#collapseMomo" aria-expanded="false" aria-controls="collapseMomo">
                                                                MoMo
                                                        </a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseMomo" className="collapse">
                                                        <div className="panel-title">
                                                            {
                                                                show_error ? 'Vui lòng điền thông tin trước!' :
                                                                <div>
                                                                    <img src="https://developers.momo.vn/v2/images/logo.png" width="50" onClick={handlerMomo}
                                                                    style={{ cursor: 'pointer' }} />
                                                                    <MoMo 
                                                                        orderID={orderID}
                                                                        total={total_price}
                                                                        />
                                                                </div>  
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                }
            </div>
        </div>

    );
}

export default Checkout;