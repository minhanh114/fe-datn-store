import "./CSS/animate.css";
import "./CSS/helper.css";
import "./CSS/magnific-popup.css";
import "./CSS/material-design-iconic-font.min.css";
import "./CSS/meanmenu.css";
import "./CSS/nice-select.css";
import "./CSS/slick.css";
import "./CSS/venobox.css";
import "./CSS/style.css";
import "./App.css";

import { BrowserRouter, Route, Switch } from "react-router-dom";
import { lazy, Suspense } from "react";
import Header from "./Share/Header";
import Footer from "./Share/Footer";
import Checkout from "./Checkout/Checkout";
import OrderFail from "./Order/OrderFail";
import OrderMomo from "./Order/OrderMomo";
import ForgotPassword from "./Auth/ForgetPassword";
import ResetPassword from "./Auth/ResetPassword";

const Home = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./Home/Home")), 500);
  });
});

const Shop = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./Shop/Shop")), 500);
  });
});

const Detail_Product = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./DetailProduct/Detail_Product")), 500);
  });
});

const Cart = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./Cart/Cart")), 500);
  });
});

const OrderSuceess = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./Order/OrderSuccess")), 900);
  });
});

const Favorite = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./Favorite/Favorite")), 500);
  });
});

const Event = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./Event/Event")), 500);
  });
});

const DetailEvent = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./Event/DetailEvent")), 500);
  });
});

const Contact = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./Contact/Contact")), 500);
  });
});

const SignIn = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./Auth/SignIn")), 500);
  });
});

const SignUp = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./Auth/SignUp")), 500);
  });
});

const History = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./History/History")), 200);
  });
});

const Profile = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./Profile/Profile")), 500);
  });
});

const Search = lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./Search/Search")), 500);
  });
});

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Suspense //asynchronous data fetching
          fallback={
            // Load page effect
            <div className="sk-cube-grid">
              <div className="sk-cube sk-cube1"></div>
              <div className="sk-cube sk-cube2"></div>
              <div className="sk-cube sk-cube3"></div>
              <div className="sk-cube sk-cube4"></div>
              <div className="sk-cube sk-cube5"></div>
              <div className="sk-cube sk-cube6"></div>
              <div className="sk-cube sk-cube7"></div>
              <div className="sk-cube sk-cube8"></div>
              <div className="sk-cube sk-cube9"></div>
            </div>
          }
        >
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/shop/:id" component={Shop} />
            <Route path="/detail/:id" component={Detail_Product} />
            <Route path="/cart" component={Cart} />
            <Route path="/checkout" component={Checkout} />
            <Route path="/favorite" component={Favorite} />

            <Route exact path="/event" component={Event} />
            <Route path="/event/:id" component={DetailEvent} />


            <Route path="/contact" component={Contact} />
            <Route path="/signin" component={SignIn} />
            <Route path="/signup" component={SignUp} />
            <Route path="/success" component={OrderSuceess} />
            <Route path="/fail" component={OrderFail} />
            <Route path="/momo" component={OrderMomo} />
            <Route path="/history" component={History} />
            <Route path="/profile/:id" component={Profile} />
            <Route path="/search" component={Search} />
            <Route path="/forgetpassword" component={ForgotPassword} />
            <Route path="/resetpassword" component={ResetPassword} />
          </Switch>
        </Suspense>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
