import React, { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import queryString from 'query-string';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { changeCount } from '../Redux/Action/ActionCount';

import CartsLocal from '../Share/CartsLocal';

import Product from '../API/Product';
import './Search.css'

Search.propTypes = {

};

function Search(props) {

    const [products, set_products] = useState([])
    const [page, set_page] = useState(1)
    const count_change = useSelector(state => state.Count.isLoad)
    const dispatch = useDispatch()

    const [show_load, set_show_load] = useState(true)
    useEffect(() => {

        setTimeout(() => {
            const fetchData = async () => {

                const params = {
                    page: page,
                    count: '6',
                    search: sessionStorage.getItem('search')
                }

                const query = '?' + queryString.stringify(params)

                const response = await Product.get_search_list(query)

                if (response.length < 1) {
                    set_show_load(false)
                }

                set_products(prev => [...prev, ...response])

            }

            fetchData()
        }, 500)

    }, [page])

    const [show_success, set_show_success] = useState(false)
    const [show_success_t, set_show_success_t] = useState(false)

    // Hàm này dùng để thêm vào giỏ hàng
    const handler_addcart = (datas) => {
        /*  const e = ''
         e.preventDefault() */
        const data = {
            id_cart: Math.random().toString(),
            id_product: datas._id,
            name_product: datas.name_product,
            price_product: datas.price_product,
            count: 1,
            image: datas.image,
            size: 'S',
        }

        CartsLocal.addProduct(data)

        /*  const action_count_change = changeCount(count_change)
         dispatch(action_count_change) */

    }

    return (

        <div className="content-wraper pt-60 pb-60">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="shop-top-bar">
                            <div className="product-select-box">
                                <div className="product-short">
                                    <p>Sắp xếp:</p>
                                    <select className="nice-select">
                                        <option value="trending">Liên quan</option>
                                        {/* <option value="rating">Giá (Cao &gt; Thấp)</option>
                                        <option value="rating">Giá (Thấp &gt; Cao)</option> */}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="shop-products-wrapper">
                            <div className="row">
                                <div className="col">
                                    <InfiniteScroll
                                        style={{ overflow: 'none' }}
                                        dataLength={products.length}
                                        next={() => set_page(page + 1)}
                                        hasMore={true}
                                        loader={show_load ? <div className="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                                            : <h4 className="text-center" style={{ paddingTop: '3rem', color: '#00CDCD' }}>Bạn có thể xem tất cả ở đây</h4>}
                                    >
                                        {
                                            products && products.map(value => (
                                                <div className="row product-layout-list" key={value._id}>
                                                    <div className="col-lg-3 col-md-5 ">
                                                        <div className="product-image">
                                                            <Link to={`/detail/${value._id}`}>
                                                                <img src={value.image} alt="Li's Product Image" />
                                                            </Link>
                                                            <span className="sticker">Mới</span>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-5 col-md-7">
                                                        <div className="product_desc">
                                                            <div className="product_desc_info">
                                                                <div className="product-review">
                                                                    <h5 className="manufacturer">
                                                                        <h4><a href={`/detail/${value._id}`}>{value.name_product}</a></h4>
                                                                    </h5>
                                                                    <div className="rating-box">
                                                                        <ul className="rating">
                                                                            <li><i className="fa fa-star" /></li>
                                                                            <li><i className="fa fa-star" /></li>
                                                                            <li><i className="fa fa-star" /></li>
                                                                            <li><i className="fa fa-star" /></li>
                                                                            <li><i className="fa fa-star" /></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                {/*                                                                 <h4><a className="product_name" href="product-details.html">{value.name_product}</a></h4>
 */}                                                                <div className="price-box">
                                                                    <span className="new-price">{new Intl.NumberFormat('vi-VN', { style: 'decimal', decimal: 'VND' }).format(value.price_product) + ' VNĐ'}</span>
                                                                </div>
                                                                <p>
                                                                    <span>
                                                                        {value.describe}
                                                                    </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-4">
                                                        <div className="shop-add-action mb-xs-30">
                                                            <ul className="add-actions-link">
                                                                <li className="add-cart">
                                                                    <a href="#" onClick={() => {
                                                                        handler_addcart(value);
                                                                        const action_count_change = changeCount(count_change);
                                                                        dispatch(action_count_change);
                                                                        set_show_success_t(true);
                                                                        setTimeout(() => {
                                                                            set_show_success_t(false);
                                                                        }, 1000);
                                                                    }}>
                                                                        {show_success_t && (
                                                                            <div className="modal_success">
                                                                                <div className="group_model_success pt-3">
                                                                                    <div className="text-center p-2">
                                                                                        <i className="fa fa-bell fix_icon_bell" style={{ fontSize: '20px', color: '#fff' }}></i>
                                                                                    </div>
                                                                                    <h4 className="text-center p-3" style={{ color: '#fff' }}>Bạn Đã Thêm Hàng Thành Công!</h4>
                                                                                </div>
                                                                            </div>
                                                                        )}
                                                                        Thêm giỏ hàng
                                                                    </a>
                                                                </li>
                                                                <li className="wishlist"><a href="wishlist.html"><i className="fa fa-heart-o" /> Thêm vào yêu thích</a></li>
                                                                <li><a className="quick-view" data-toggle="modal" data-target={`#${value._id}`} href="#"><i className="fa fa-eye" /> Xem nhanh</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))
                                        }
                                    </InfiniteScroll>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {
                    products && products.map(value => (
                        <div className="modal fade modal-wrapper" key={value._id} id={value._id} >
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-body">
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <div className="modal-inner-area row">
                                            <div className="col-lg-5 col-md-6 col-sm-6">
                                                <div className="product-details-left">
                                                    <div className="product-details-images slider-navigation-1">
                                                        <div className="lg-image">
                                                            <img src={value.image} alt="product image" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-lg-7 col-md-6 col-sm-6">
                                                <div className="product-details-view-content pt-60">
                                                    <div className="product-info">
                                                        <h2>{value.name_product}</h2>
                                                        <div className="rating-box pt-20">
                                                            <ul className="rating rating-with-review-item">
                                                                <li><i className="fa fa-star-o"></i></li>
                                                                <li><i className="fa fa-star-o"></i></li>
                                                                <li><i className="fa fa-star-o"></i></li>
                                                                <li className="no-star"><i className="fa fa-star-o"></i></li>
                                                                <li className="no-star"><i className="fa fa-star-o"></i></li>
                                                            </ul>
                                                        </div>
                                                        <div className="price-box pt-20">
                                                            <span className="new-price new-price-2">{new Intl.NumberFormat('vi-VN', { style: 'decimal', decimal: 'VND' }).format(value.price_product) + ' VNĐ'}</span>
                                                        </div>
                                                        <div className="product-desc">
                                                            <p>
                                                                <span>
                                                                    {value.describe}
                                                                </span>
                                                            </p>
                                                        </div>
                                                        <div className="single-add-to-cart">
                                                            <form action="#" className="cart-quantity">
                                                                <button className="add-to-cart" onClick={() => {
                                                                    handler_addcart(value)
                                                                    const action_count_change = changeCount(count_change)
                                                                    dispatch(action_count_change)
                                                                    set_show_success(true),
                                                                        setTimeout(() => {
                                                                            set_show_success(false)
                                                                        }, 1000)
                                                                }}>
                                                                    {show_success &&
                                                                        <div className="modal_success">
                                                                            <div className="group_model_success pt-3">
                                                                                <div className="text-center p-2">
                                                                                    <i className="fa fa-bell fix_icon_bell" style={{ fontSize: '20px', color: '#fff' }}></i>
                                                                                </div>
                                                                                <h4 className="text-center p-3" style={{ color: '#fff' }}>Bạn Đã Thêm Hàng Thành Công!</h4>
                                                                            </div>
                                                                        </div>
                                                                    }
                                                                    Thêm vào giỏ hàng
                                                                </button>
                                                            </form>
                                                        </div>
                                                        {/*   <div className="single-add-to-cart">
                                                            <form action="#" className="cart-quantity">
                                                                <div className="quantity">
                                                                    <label>Số lượng</label>
                                                                    <div className="cart-plus-minus">
                                                                        <input className="cart-plus-minus-box" value={count} type="text" onChange={(e) => set_count(e.target.value)} />
                                                                        <div className="dec qtybutton" onClick={downCount}><i className="fa fa-angle-down"></i></div>
                                                                        <div className="inc qtybutton" onClick={upCount}><i className="fa fa-angle-up"></i></div>
                                                                    </div>
                                                                </div>
                                                                <button className="add-to-cart" type="submit" >Thêm vào giỏ hàng</button>
                                                            </form>
                                                        </div> */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                }

            </div>
        </div>
    )
}

export default Search;