import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import queryString from 'query-string';
import { useAlert } from 'react-alert';
import { isEmpty } from 'validator';
import User from '../API/User';
import { addSession } from '../Redux/Action/ActionSession';
import { changeCount } from '../Redux/Action/ActionCount';

SignIn.propTypes = {

};

function SignIn(props) {

    const dispatch = useDispatch()
    const alert = useAlert();

    const [email, set_email] = useState('')
    const [password, set_password] = useState('')

    const [error_email, set_error_email] = useState(false)
    const [error_password, set_error_password] = useState(false)

    const [redirect, set_redirect] = useState(false)

    const [validationMsg, setValidationMsg] = useState('');
    const [showPassword, setShowPassword] = useState(false);


    // Get carts từ redux khi user chưa đăng nhập
    const carts = useSelector(state => state.Cart.listCart)

    // Get isLoad từ redux để load lại phần header
    const count_change = useSelector(state => state.Count.isLoad)
    const validateAll = () => {
        let msg = {}
        if (isEmpty(email)) {
            msg.email = "Email không được để trống"
        }
        if (isEmpty(password)) {
            msg.password = "Password không được để trống"
        }
        setValidationMsg(msg)
        if (Object.keys(msg).length > 0) return false;
        return true;
    }
    const signIn = (e) => {
        const isValid = validateAll();
        if (!isValid) return
        e.preventDefault()

        handler_signin()
    }
    const handler_signin = (e) => {

        const fetchData = async () => {

            const params = {
                email,
                password
            }

            const query = '?' + queryString.stringify(params)

            const response = await User.Get_Detail_User(query)

            if (response === "Khong Tìm Thấy User") {
                set_error_email(true)
            } else {
                if (response === "Sai Mat Khau") {
                    set_error_email(false)
                    set_error_password(true)
                } else {
                    alert.success("Đăng nhập thành công !")
                    const action = addSession(response._id)
                    dispatch(action)

                    sessionStorage.setItem('id_user', response._id)

                    const action_count_change = changeCount(count_change)
                    dispatch(action_count_change)

                    set_redirect(true)

                }
            }

        }

        fetchData()

    }
    const isValidEmail = (email) => {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    }

    return (
        <div>
            <div className="breadcrumb-area">
                <div className="container">
                    <div className="breadcrumb-content">
                        <ul>
                            <li><Link to="/">Trang chủ</Link></li>
                            <li className="active">Đăng nhập</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="page-section mb-60">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 col-md-12 col-xs-12 col-lg-6 mb-30 mr_signin">
                            <form action="#" >
                                <div className="login-form">
                                    <h4 className="login-title" style={{color: '#555555'}}>Đăng nhập</h4>
                                    {
                                        <p className="form-text text-danger">{validationMsg.api}</p>
                                    }
                                    <div className="row">
                                        <div className="col-md-12 col-12 mb-20">
                                            <label>Email *</label>
                                            <input className="mb-0" type="email" placeholder="Email" value={email} onChange={(e) => set_email(e.target.value)} />
                                            <p className="form-text text-danger">{validationMsg.email}</p>
                                            {
                                                error_email && <span style={{ color: 'red' }}>* Sai Email!</span>
                                            }
                                            {email && !isValidEmail(email) && <span style={{ color: 'red' }}>* Địa chỉ email không hợp lệ!</span>}

                                        </div>
                                        <div className="col-12 mb-20">
                                            <label>Mật khẩu *</label>
                                            <div className="input-group">
                                                <input
                                                    className="form-control"
                                                    name="password"
                                                    type={showPassword ? "text" : "password"}
                                                    value={password}
                                                    onChange={(e) => set_password(e.target.value)}
                                                    placeholder="Nhập mật khẩu"
                                                />
                                                <div className="input-group-append">
                                                    <span
                                                        className="input-group-text"
                                                        onClick={() => setShowPassword(!showPassword)}
                                                        style={{ cursor: 'pointer' }}
                                                    >
                                                        {showPassword ? "Ẩn" : "Hiện"}
                                                    </span>
                                                </div>
                                            </div>
                                            {
                                                error_password && <span style={{ color: 'red' }}>* Sai mật khẩu!</span>
                                            }
                                        </div>
                                        <div className="col-md-8">
                                            <div className="check-box d-inline-block ml-0 ml-md-2 mt-10">
                                                <Link to="/signup">Bạn chưa có tài khoản?</Link>
                                            </div>
                                        </div>
                                        <div className="col-md-4 mt-10 mb-20 text-left text-md-right">
                                            <Link to="/forgetpassword"> Quên mật khẩu?</Link>
                                        </div>
                                        <div className="col-md-12">
                                            {
                                                redirect && <Redirect to="/" />
                                            }
                                            <button className="register-button mt-0" style={{ cursor: 'pointer' }} onClick={signIn}>Đăng nhập</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SignIn;