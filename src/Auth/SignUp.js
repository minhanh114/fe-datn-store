import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import User from '../API/User';
import { useForm } from "react-hook-form";
import { useAlert } from 'react-alert'

SignUp.propTypes = {

};

function SignUp(props) {

    const { register, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = data => console.log(data);
    const alert = useAlert();

    const [fullname, set_fullname] = useState('')
    const [username, set_username] = useState('')
    const [password, set_password] = useState('')
    const [confirm, set_confirm] = useState('')
    const [email, set_email] = useState('')

    const [show_success, set_show_success] = useState(false)

    const [errorEmail, setEmailError] = useState(false)
    const [errorFullname, setFullnameError] = useState(false)
    const [errorUsername, setUsernameError] = useState(false)
    const [errorPassword, setPasswordError] = useState(false)
    const [errorConfirm, setConfirmError] = useState(false)
    const [errorCheckPass, setCheckPass] = useState(false)

    const [username_exist, set_username_exist] = useState(false)
    const [email_exist, set_email_exist] = useState(false)

    const handler_signup = (e) => {

        e.preventDefault()

        if (!email) {
            setEmailError(true)
            return
        } else {
            setEmailError(false)
        }

        if (!fullname) {
            setFullnameError(true)
            setUsernameError(false)
            setPasswordError(false)
            setConfirmError(false)
            return
        } else {
            setFullnameError(false)
            setUsernameError(false)
            setPasswordError(false)
            setConfirmError(false)

            if (!username) {
                setFullnameError(false)
                setUsernameError(true)
                setPasswordError(false)
                setConfirmError(false)
                return
            } else {
                setFullnameError(false)
                setUsernameError(false)
                setPasswordError(false)
                setConfirmError(false)

                if (!password) {
                    setFullnameError(false)
                    setUsernameError(false)
                    setPasswordError(true)
                    setConfirmError(false)
                    return
                } else {
                    setFullnameError(false)
                    setUsernameError(false)
                    setPasswordError(false)
                    setConfirmError(false)

                    if (!confirm) {
                        setFullnameError(false)
                        setUsernameError(false)
                        setPasswordError(false)
                        setConfirmError(true)
                        return
                    } else {
                        setFullnameError(false)
                        setUsernameError(false)
                        setPasswordError(false)
                        setConfirmError(false)

                        if (password !== confirm) {
                            setFullnameError(false)
                            setUsernameError(false)
                            setPasswordError(false)
                            setConfirmError(false)
                            setCheckPass(true)
                            return
                        } else {
                            setConfirmError(false)
                            setCheckPass(false)

                            const fetchData = async () => {

                                const data = {
                                    email: email,
                                    username: username,
                                    password: password,
                                    fullname: fullname,
                                    id_permission: '65389cb5d833104afc9959fe'
                                }

                                const response = await User.Post_User(data)

                                if (response === 'Username Da Ton Tai') {
                                    set_username_exist(true)

                                } else if (response === 'Email Da Ton Tai') {
                                    set_email_exist(true)
                                } else {
                                    alert.success("Đăng ký thành công !");
                                }
                            }

                            fetchData()

                            set_fullname('')
                            set_username('')
                            set_password('')
                            set_fullname('')
                            set_confirm('')

                        }

                    }

                }
            }

        }

        setTimeout(() => {
            set_show_success(false)
        }, 1500)

    }

    const isValidEmail = (email) => {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    }

    return (
        <div>
            <div className="breadcrumb-area">
                <div className="container">
                    <div className="breadcrumb-content">
                        <ul>
                            <li><Link to="/">Trang chủ</Link></li>
                            <li className="active">Đăng ký</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="page-section mb-60">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 col-md-12 col-lg-6 col-xs-12 mr_signin">
                            <form action="#">
                                <div className="login-form">
                                    <h4 className="login-title" style={{color: '#555555'}}>Đăng ký</h4>
                                    <div className="row">
                                        <div className="col-md-12 mb-20">
                                            <label>Email *</label>
                                            <input className="mb-0" type="text" placeholder="Email" value={email} onChange={(e) => set_email(e.target.value)} />
                                            {
                                                errorEmail && <span style={{ color: 'red' }}>* Email bắt buộc nhập!</span>
                                            }
                                            {
                                                email_exist && <span style={{ color: 'red' }}>* Email đã tồn tại!</span>
                                            }
                                            {email && !isValidEmail(email) && <span style={{ color: 'red' }}>* Địa chỉ email không hợp lệ!</span>}

                                        </div>
                                        <div className="col-md-12 mb-20">
                                            <label>Tên *</label>
                                            <input className="mb-0" type="text" placeholder="Tên" value={fullname} onChange={(e) => set_fullname(e.target.value)} />
                                            {
                                                errorFullname && <span style={{ color: 'red' }}>* Tên bắt buộc nhập!</span>
                                            }
                                        </div>
                                        <div className="col-md-12 mb-20">
                                            <label>Username *</label>
                                            <input className="mb-0" type="text" placeholder="Username" value={username} onChange={(e) => set_username(e.target.value)} />
                                            {
                                                errorUsername && <span style={{ color: 'red' }}>* Username bắt buộc nhập!</span>
                                            }
                                            {
                                                username_exist && <span style={{ color: 'red' }}>* Username đã tồn tại!</span>
                                            }
                                        </div>
                                        <div className="col-md-6 mb-20">
                                            <label>Mật khẩu *</label>
                                            <input className="mb-0" type="password" placeholder="Mật khẩu" value={password} onChange={(e) => set_password(e.target.value)} />
                                            {
                                                errorPassword && <span style={{ color: 'red' }}>* Password bắt buộc nhập!</span>
                                            }
                                        </div>
                                        <div className="col-md-6 mb-20">
                                            <label>Xác nhận mật khẩu *</label>
                                            <input className="mb-0" type="password" placeholder="Xác nhận mật khẩu" value={confirm} onChange={(e) => set_confirm(e.target.value)} />
                                            {
                                                errorConfirm && <span style={{ color: 'red' }}>* Xác nhận mật khẩu bắt buộc nhập!</span>
                                            }
                                            {
                                                errorCheckPass && <span style={{ color: 'red' }}>* Kiểm tra lại mật khẩu!</span>
                                            }
                                        </div>
                                        <div className="col-md-12 mb-20">
                                            <div className="d-flex justify-content-end">
                                                <Link to="/signin">Bạn có muốn đăng nhập?</Link>
                                            </div>
                                        </div>
                                        <div className="col-12">
                                            <button className="register-button mt-0" style={{ cursor: 'pointer' }} onClick={handler_signup}>Đăng ký</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SignUp;