// ForgotPassword.js
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import User from '../API/User';

const containerStyle = {
    maxWidth: '400px',
    margin: '100px auto',
    padding: '20px',
    border: '1px solid #ddd',
    borderRadius: '8px',
    boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
    backgroundColor: '#fff',
};

const titleStyle = {
    color: '#363f4d',
    fontSize: '2rem',
    marginBottom: '20px',
};

const inputStyle = {
    width: '100%',
    padding: '10px',
    margin: '10px 0',
    boxSizing: 'border-box',
    borderRadius: '4px',
    border: '1px solid #ddd',
};

const buttonStyle = {
    width: '100%',
    padding: '10px',
    backgroundColor: '#363f4d',
    color: '#fff',
    border: 'none',
    borderRadius: '4px',
    cursor: 'pointer',
};

const hoverButtonStyle = {
    backgroundColor: '#00CDCD',
};

const messageStyle = {
    fontSize: '1.2rem',
    margin: '10px 0',
    color: '#555',
};

const ForgotPassword = () => {
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');
    const [isSuccess, setIsSuccess] = useState(false);
    const [isHovered, setIsHovered] = useState(false);


    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await User.Forgot_Password({ email: email, isClient: true })
            if (response.success === true) {
                setMessage(response.msg);
                setIsSuccess(true);
            } else {
                setMessage(response.msg);
                setIsSuccess(false);
            }
            console.log(response.msg);
        } catch (error) {
            setMessage('Email không hợp lệ !');
            setIsSuccess(false);
        }
    };

    return (
        <div style={containerStyle}>
            <h2 style={titleStyle}>Quên mật khẩu</h2>
            <form onSubmit={handleSubmit}>
                <label>
                    Email:
                    <input
                        style={inputStyle}
                        type="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </label>
                <button
                    style={{ ...buttonStyle, ...(isHovered ? hoverButtonStyle : {}) }}
                    type="submit"
                    onMouseEnter={() => setIsHovered(true)}
                    onMouseLeave={() => setIsHovered(false)}
                >
                    Gửi yêu cầu
                </button>
            </form>
            <p style={{ ...messageStyle, color: isSuccess ? 'green' : 'red' }}>{message}</p>
            <p>
                Đã nhớ mật khẩu? <Link to="/signin">Đăng nhập ngay</Link>
            </p>
        </div>
    );
};

export default ForgotPassword;
