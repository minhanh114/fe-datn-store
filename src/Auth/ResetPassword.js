import React, { useState } from 'react';
import User from '../API/User';

const containerStyle = {
    maxWidth: '400px',
    margin: '100px auto',
    padding: '20px',
    border: '1px solid #ddd',
    borderRadius: '8px',
    boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
    backgroundColor: '#fff',
};

const titleStyle = {
    color: '#363f4d',
    fontSize: '2rem',
    marginBottom: '20px',
};

const inputStyle = {
    width: '100%',
    padding: '10px',
    margin: '10px 0',
    boxSizing: 'border-box',
    borderRadius: '4px',
    border: '1px solid #ddd',
};

const buttonStyle = {
    width: '100%',
    padding: '10px',
    backgroundColor: '#363f4d',
    color: '#fff',
    border: 'none',
    borderRadius: '4px',
    cursor: 'pointer',
};
const hoverButtonStyle = {
    backgroundColor: '#00CDCD',
};
const messageStyle = {
    fontSize: '1.2rem',
    margin: '10px 0',
    color: '#555',
};

const ResetPassword = () => {
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [message, setMessage] = useState('');
    const [error, setError] = useState('');
    const [isHovered, setIsHovered] = useState(false);

    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    };

    const handleConfirmPasswordChange = (e) => {
        setConfirmPassword(e.target.value);
    };

    const handleResetPassword = async () => {
        try {
            if (!password || !confirmPassword) {
                setError('Vui lòng nhập cả 2 trường mật khẩu.');
                return;
            }

            if (password !== confirmPassword) {
                setError('Mật khẩu không khớp.');
                return;
            }

            const token = new URLSearchParams(window.location.search).get('token');
            const response = await User.Reset_Password(token, { password });

            if (response.success === true) {
                setMessage(response.msg);
                if (error.length > 0) {
                    setError('')
                }
            } else {
                setError(response.msg);
            }
        } catch (error) {
            setError('Có lỗi xảy ra trong quá trình yêu cầu !');
        }
    };

    return (
        <div style={containerStyle}>
            <h2 style={titleStyle}>Đặt lại mật khẩu</h2>
            {message && <p style={{ ...messageStyle, color: 'green' }}>{message}</p>}
            {error && <p style={{ ...messageStyle, color: 'red' }}>{error}</p>}
            <label>Mật khẩu mới:</label>
            <input style={inputStyle} type="password" value={password} onChange={handlePasswordChange} />
            <label>Xác nhận mật khẩu:</label>
            <input style={inputStyle} type="password" value={confirmPassword} onChange={handleConfirmPasswordChange} />
            <button
                style={{ ...buttonStyle, ...(isHovered ? hoverButtonStyle : {}) }}
                onMouseEnter={() => setIsHovered(true)}
                onMouseLeave={() => setIsHovered(false)}
                onClick={handleResetPassword}
            >
                Gửi yêu cầu
            </button>
        </div>
    );
};

export default ResetPassword;
