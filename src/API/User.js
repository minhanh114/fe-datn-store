import axiosClient from './axiosClient'

const User = {

    Get_All_User: () => {
        const url = '/api/User'
        return axiosClient.get(url)
    },

    Get_User: (id) => {
        const url = `/api/User/${id}`
        return axiosClient.get(url)
    },

    Put_User: (data) => {
        const url = `/api/User`
        return axiosClient.put(url, data)
    },

    Get_Detail_User: async (query) => {
        const url = `/api/User/detail/login${query}`
        return await axiosClient.get(url)
    },

    Post_User: async (data) => {
        const url = '/api/User';
        return await axiosClient.post(url, data)
    },

    Forgot_Password: async (data) => {
        const url = '/api/User/forgot-password';
        return await axiosClient.post(url, data)
    },

    Reset_Password: async (token, data) => {
        const url = `/api/User/reset-password?token=${token}`;
        return await axiosClient.post(url, data)
    }

}

export default User