import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom'

OrderSuccess.propTypes = {};

const containerStyle = {
    maxWidth: '600px',
    margin: '100px auto',
    textAlign: 'center',
    padding: '20px',
    border: '1px solid #ddd',
    borderRadius: '8px',
    boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
    backgroundColor: '#fff',
};

const titleStyle = {
    color: '#2ecc71',
    fontSize: '2rem',
    marginBottom: '10px',
};

const messageStyle = {
    fontSize: '1.2rem',
    color: '#555',
};

function OrderSuccess(props) {
    const history = useHistory();
    const [countDown, setCountDown] = useState(5);
    useEffect(() => {
        const redirectTimeout = setTimeout(() => {
            if (countDown === 0) {
                history.push('/history'); // Redirect to the '/history' path after 5 seconds
            } else {
                setCountDown(countDown - 1);
            }
        }, 1000);
        // Cleanup the timeout to avoid memory leaks
        return () => clearTimeout(redirectTimeout);
    }, [countDown, history]);
    return (
        <div style={containerStyle}>
            <h1 style={titleStyle}>Đặt hàng thành công!</h1>
            <p style={messageStyle}>Vui lòng kiểm tra Email để biết thông tin chi tiết.</p>
            <p style={{ fontSize: '1rem' }}>Sau{' '}
                <span style={{ color: '#2ecc71' }}>
                    {countDown}s
                </span>{' '} nữa sẽ tự động chuyển đến danh sách đơn hàng của bạn.</p>
        </div>
    );
}

export default OrderSuccess;
