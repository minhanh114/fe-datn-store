import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom'

OrderFail.propTypes = {

};
const containerStyle = {
    maxWidth: '600px',
    margin: '100px auto',
    textAlign: 'center',
    padding: '20px',
    border: '1px solid #ddd',
    borderRadius: '8px',
    boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
    backgroundColor: '#fff',
};

const titleStyle = {
    color: '#f95959',
    fontSize: '2rem',
    marginBottom: '10px',
};

const messageStyle = {
    fontSize: '1.2rem',
    color: '#555',
};
function OrderFail(props) {
    const history = useHistory();
    const [countDown, setCountDown] = useState(5);
    useEffect(() => {
        const redirectTimeout = setTimeout(() => {
            if (countDown === 0) {
                history.push('/'); // Redirect to the '/' path after 5 seconds
            } else {
                setCountDown(countDown - 1);
            }
        }, 1000);

        return () => clearTimeout(redirectTimeout);
    })
    return (
        <div style={containerStyle}>
            <h1 style={titleStyle}>Quá trình đặt hàng thất bại !</h1>
            <span style={messageStyle}>Vui lòng kiểm tra lại thông tin hoặc thử lại sau.</span>
            <p style={{ fontSize: '1rem' }}>Sau{' '}
                <span style={{ color: '#f95959' }}>
                    {countDown}s
                </span>{' '} nữa sẽ tự động chuyển trang chủ.</p>
        </div>
    );
}

export default OrderFail;