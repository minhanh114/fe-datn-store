import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import bg1 from '../CSS/Image/1.jpg'
import bg2 from '../CSS/Image/2.jpg'
import bg3 from '../CSS/Image/3.jpg'
import bg5 from '../CSS/Image/5.jpg'
import bg6 from '../CSS/Image/6.jpg'
import bg7 from '../CSS/Image/7.jpg'
import Home_Category from './Component/Home_Category';
import Home_Product from './Component/Home_Product';
import Product from '../API/Product';
import { changeCount } from '../Redux/Action/ActionCount';
import { useDispatch, useSelector } from 'react-redux';
import CartsLocal from '../Share/CartsLocal';
import SaleAPI from '../API/SaleAPI';
import Products from '../Shop/Component/Products';
import queryString from 'query-string';
import Pagination from '../Shop/Component/Pagination';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


Home.propTypes = {

};

function Home(props) {
    const [products, setProducts] = useState([])
    const [totalPage, setTotalPage] = useState()
    const [pageCheck, setPageCheck] = useState();

    // state dùng để thay đổi và hiển thị modal
    const [id_modal, set_id_modal] = useState('')

    const [product_detail, set_product_detail] = useState([])

    const dispatch = useDispatch()

    const [priceSale, setPriceSale] = useState(0)

    const GET_id_modal = (value, price) => {

        set_id_modal(value)

        setPriceSale(price)

    }

    const [pagination, setPagination] = useState({
        page: '1',
        count: '9',
        search: '',
        category: "all"
    })

    const [sale, setSale] = useState([])

    const handlerChangePage = (value) => {
        setPageCheck(value);
        console.log("Value: ", value)

        //Sau đó set lại cái pagination để gọi chạy làm useEffect gọi lại API pagination
        setPagination({
            page: value,
            count: pagination.count,
            search: pagination.search,
            category: "all"
        })

        window.scrollTo(0, 0);
    }

    useEffect(() => {

        const fetchData = async () => {
            if (pageCheck) {
                let section = document.getElementById("scroll-to-top");
                if (section) {
                    section.scrollIntoView({ behavior: 'smooth' });
                }
            }

            const params = {
                page: pagination.page,
                count: pagination.count,
                search: pagination.search,
                category: "all"
            }

            const query = '?' + queryString.stringify(params)

            const response = await Product.Get_Pagination(query)

            setProducts(response)


            // Gọi API để tính tổng số trang cho từng loại sản phẩm
            const params_total_page = {
                id_category: "all"
            }

            const query_total_page = '?' + queryString.stringify(params_total_page)

            const response_total_page = await Product.Get_Category_Product(query_total_page)

            //Tính tổng số trang = tổng số sản phẩm / số lượng sản phẩm 1 trang
            const totalPage = Math.ceil(parseInt(response_total_page.length) / parseInt(pagination.count))
            console.log(totalPage)

            setTotalPage(totalPage)

            // window.scrollTo(0, 0);

        }
        fetchData()

    }, [pagination]);

    useEffect(() => {

        if (id_modal !== '') {

            const fetchData = async () => {

                const response = await Product.Get_Detail_Product(id_modal)

                set_product_detail(response)

            }

            fetchData()

        }

    }, [id_modal])


    // Get count từ redux khi user chưa đăng nhập
    const count_change = useSelector(state => state.Count.isLoad)
    const [show_success, set_show_success] = useState(false)

    // Hàm này dùng để thêm vào giỏ hàng
    const handler_addcart = (e) => {

        e.preventDefault()

        const data = {
            id_cart: Math.random().toString(),
            id_product: id_modal,
            name_product: product_detail.name_product,
            price_product: product_detail.price_product,
            count: 1,
            image: product_detail.image,
            size: 'S',
        }

        CartsLocal.addProduct(data)

        const action_count_change = changeCount(count_change)
        dispatch(action_count_change)

    }

    const sliderRef = useRef(null);

    useEffect(() => {
        // Chạy slickNext khi component được mount
        if (sliderRef.current) {
            sliderRef.current.slickNext();
        }
    }, []); // [] đảm bảo hàm chỉ chạy sau khi component được mount

    const settings = {
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 500, // Đặt thời gian giữa các lượt chuyển động
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
        speed: 100, // Đặt tốc độ chuyển động của slider
        afterChange: (index) => {
            // Slider đã được khởi tạo, có thể sử dụng các phương thức như slickNext() ở đây
        },
    };


    return (
        // <div className="container-fluid">
        <div className="container">
            <div className="slider-with-banner">
                <div className="row">
                    <div className="col-lg-8 col-md-8">
                        <Slider {...settings} ref={sliderRef}>
                            <div id="carouselExample" className="carousel slide" data-ride="carousel">
                                <div className="carousel-inner">
                                    <div className="carousel-item active">
                                        <div className="single-slide align-center-left animation-style-01 bg-1"
                                            style={{ backgroundImage: `url(http://thegioiinan.com/images/images/The_gioi_in_an_20170412162130431roll_ups.jpg)` }}>
                                            <div className="slider-progress"></div>
                                        </div>
                                    </div>
                                    <div className="carousel-item">
                                        <div className="single-slide align-center-left animation-style-01 bg-2"
                                            style={{ backgroundImage: `url(https://hips.hearstapps.com/hmg-prod/images/classic-accessories-1516305397.jpg?crop=1.00xw:1.00xh;0,0&resize=1200:*)` }}>
                                            <div className="slider-progress"></div>
                                        </div>
                                    </div>
                                    <div className="carousel-item">
                                        <div className="single-slide align-center-left animation-style-01 bg-3"
                                            style={{ backgroundImage: `url(https://intphcm.com/data/upload/banner-thoi-trang.jpg)` }}>
                                            <div className="slider-progress"></div>
                                        </div>
                                    </div>
                                    <div className="carousel-item">
                                        <div className="single-slide align-center-left animation-style-01 bg-3"
                                            style={{ backgroundImage: `url(https://thietkewebchuyen.com/images/mau-banner-thiet-ke-gia-re-dep-tai-tphcm/thiet-ke-banner-shop-thoi-trang-quan-ao-son-moi-phu-kien-2.jpg)` }}>
                                            <div className="slider-progress"></div>
                                        </div>
                                    </div>
                                </div>
                                <a className="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span className="sr-only">Previous</span>
                                </a>
                                <a className="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span className="sr-only">Next</span>
                                </a>
                            </div>
                        </Slider>
                    </div>
                    <div className="col-lg-4 col-md-4 text-center pt-xs-30" >
                        <div className="li-banner">
                            <a href="#">
                                <img src="https://i.pinimg.com/564x/1a/0e/bb/1a0ebbbad332362d19734a850b273746.jpg" alt="" height={'200px'} />
                            </a>
                        </div>
                        <div className="li-banner mt-15 mt-sm-30 mt-xs-30">
                            <a href="#">
                                <img src="https://img.freepik.com/premium-photo/flat-lay-trendy-creative-feminine-accessories-arrangement-purse-hat-sunglasses-female-accessories_408798-5917.jpg?w=740" alt="" height={'232px'} />
                            </a>
                        </div>
                    </div>
                </div>
            </div >

            <div className="li-static-banner" style={{ marginTop: "20px" }}>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 col-md-4 text-center">
                            <div className="single-banner">
                                <a href="#">
                                    <img src="https://image.freepik.com/free-vector/fashion-sale-banner-collection_23-2148161688.jpg" alt="Li's Static Banner" />
                                </a>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-4 text-center pt-xs-30">
                            <div className="single-banner">
                                <a href="#">
                                    <img src="https://image.freepik.com/free-vector/fashion-sale-banners_52683-11557.jpg" alt="Li's Static Banner" />
                                </a>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-4 text-center pt-xs-30">
                            <div className="single-banner">
                                <a href="#">
                                    <img src="https://image.freepik.com/free-vector/fashion-banner-design-with-shirt-bag-camera-case_83728-1865.jpg" alt="Li's Static Banner" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Home_Category GET_id_modal={GET_id_modal} />
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="li-product-tab">
                            <ul className="nav li-product-menu">
                                <li><a className="active" data-toggle="tab" href="#"><span>Sản phẩm</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="shop-products-wrapper" >
                    <div className="tab-content">
                        <div id="grid-view" className="tab-pane active" role="tabpanel">
                            <div className="product-area shop-product-area">
                                <Products products={products} />
                            </div>
                        </div>
                        <div className="paginatoin-area">
                            <div className="row">
                                <div className="col-lg-6 col-md-6">
                                </div>
                                <Pagination pagination={pagination} handlerChangePage={handlerChangePage} totalPage={totalPage} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal fade modal-wrapper" id={id_modal} >

                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div className="modal-inner-area row">
                                <div className="col-lg-5 col-md-6 col-sm-6">
                                    <div className="product-details-left">
                                        <div className="product-details-images slider-navigation-1">
                                            <div className="lg-image">
                                                <img src={product_detail.image} alt="product image" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-7 col-md-6 col-sm-6">
                                    <div className="product-details-view-content pt-60">
                                        <div className="product-info">
                                            <h2>{product_detail.name_product}</h2>
                                            <div className="rating-box pt-20">
                                                <ul className="rating rating-with-review-item">
                                                    <li><i className="fa fa-star-o"></i></li>
                                                    <li><i className="fa fa-star-o"></i></li>
                                                    <li><i className="fa fa-star-o"></i></li>
                                                    <li className="no-star"><i className="fa fa-star-o"></i></li>
                                                    <li className="no-star"><i className="fa fa-star-o"></i></li>
                                                </ul>
                                            </div>
                                            <div className="price-box pt-20">
                                                {
                                                    priceSale ? (<del className="new-price new-price-2" style={{ color: '#525252' }}>{new Intl.NumberFormat('vi-VN', { style: 'decimal', decimal: 'VND' }).format(product_detail.price_product) + ' VNĐ'}</del>) :
                                                        <span className="new-price new-price-2">{new Intl.NumberFormat('vi-VN', { style: 'decimal', decimal: 'VND' }).format(product_detail.price_product) + ' VNĐ'}</span>
                                                }
                                                <br />
                                                {
                                                    priceSale && <span className="new-price new-price-2">{new Intl.NumberFormat('vi-VN', { style: 'decimal', decimal: 'VND' }).format(priceSale) + ' VNĐ'}</span>
                                                }
                                            </div>
                                            <div className="product-desc">
                                                <p>
                                                    <span>
                                                        {product_detail.describe}
                                                    </span>
                                                </p>
                                            </div>
                                            <div className="single-add-to-cart">
                                                <form onSubmit={handler_addcart} className="cart-quantity">
                                                    <button className="add-to-cart" onClick={() => {
                                                        set_show_success(true),
                                                            setTimeout(() => {
                                                                set_show_success(false)
                                                            }, 1000)
                                                    }}>
                                                        {show_success &&
                                                            <div className="modal_success">
                                                                <div className="group_model_success pt-3">
                                                                    <div className="text-center p-2">
                                                                        <i className="fa fa-bell fix_icon_bell" style={{ fontSize: '20px', color: '#fff' }}></i>
                                                                    </div>
                                                                    <h4 className="text-center p-3" style={{ color: '#fff' }}>Bạn Đã Thêm Hàng Thành Công!</h4>
                                                                </div>
                                                            </div>
                                                        }
                                                        Thêm vào giỏ hàng
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    );
}

export default Home;