import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

Products.propTypes = {
    products: PropTypes.array,
    sort: PropTypes.string
};

Products.defaultProps = {
    products: [],
    sort: ''
}

function Products(props) {

    const { products, sort } = props

    if (sort === 'DownToUp') {
        products.sort((a, b) => {
            return a.price_product - b.price_product
        });
    }
    else if (sort === 'UpToDown') {
        products.sort((a, b) => {
            return b.price_product - a.price_product
        });
    }
    //TODO: Show Product For Home And Category
    return (
        <div className="row" >
            {
                products && products.map(value => (
                    <div className="col-lg-4 col-md-4 col-sm-6 mt-40 animate__animated animate__zoomIn col_product" key={value._id}>
                        <div className="single-product-wrap" style={{ boxShadow: "0 0 5px 0px #9E9E9E", padding: "10px 10px" }}>
                            <div className="product-image" style={{ display: "flex", height: "250px", overflow: 'hidden' }}>
                                <Link to={`/detail/${value._id}`} style={{ margin: "0 auto" }}>
                                    <img style={{ height: "250px", width: "auto", }} src={value.image} alt="Ảnh" />
                                </Link>
                            </div>
                            <div className="product_desc">
                                <div className="product_desc_info">
                                    <div className="product-review">
                                        <div style={{
                                            marginTop: '.4rem', whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"
                                        }}>
                                            <p className="manufacturer" style={{ fontSize: '1rem', color: 'gray' }} title={value.name_product}>
                                                <a href={`/detail/${value._id}`}>{value.name_product}</a>
                                            </p>
                                        </div>
                                        <div className="rating-box">
                                            <ul className="rating">
                                                <li><i className="fa fa-star-o"></i></li>
                                                <li><i className="fa fa-star-o"></i></li>
                                                <li><i className="fa fa-star-o"></i></li>
                                                <li className="no-star"><i className="fa fa-star-o"></i></li>
                                                <li className="no-star"><i className="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="price-box">
                                        <span className="new-price">{new Intl.NumberFormat('vi-VN', { style: 'decimal', decimal: 'VND' }).format(value.price_product) + ' VNĐ'}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ))
            }
        </div >
    );
}

export default Products;