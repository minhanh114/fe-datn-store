import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import CouponAPI from '../API/CouponAPI';
import './Event.css'

function DetailEvent(props) {

    const { id } = useParams()

    const [coupon, setCoupon] = useState({})

    useEffect(() => {

        const fetchData = async () => {

            const resposne = await CouponAPI.getCoupon(id)

            setCoupon(resposne)

        }

        fetchData()

    }, [id])

    return (
        <div>
            <div className="breadcrumb-area">
                <div className="container">
                    <div className="breadcrumb-content">
                        <ul>
                            <li><a href="index.html">Trang chủ</a></li>
                            <li className="active">Sự kiện</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="container" style={{ marginTop: '3rem' }}>
                <h1 className="h4_event">{coupon.describe} CÙNG DIMPLE TẬN HƯỞNG NÀO!!!</h1>
                <div style={{ marginTop: '2rem' }}>
                    <a className="a_event">Khuyến mãi</a>
                </div>
                <div style={{ marginTop: '2rem' }}>
                    <span style={{ fontSize: '1.2rem', color: '#646464', fontWeight: 'bold' }}>Cơ hội nhận ngay phiếu giảm giá đơn hàng của DIMPLE!!!</span>
                    <br />
                    <span style={{ fontSize: '1.05rem', color: 'black' }}>Chỉ cần bạn ghé ngay DIMPLE và mua đơn hàng sẽ được giảm giá theo mã code dưới đây:</span>
                    <li style={{ fontSize: '1.05rem', color: 'black' }}>Mã Code: <i style={{ color: 'red' }}>{coupon.code}</i></li>
                    <span style={{ fontSize: '1.05rem', color: 'black' }}>Bạn sẽ nhập mã code vào ô ÁP MÃ GIẢM trong giỏ hàng của mình khi tiến hành thanh toán.</span>
                    <br />
                    <span style={{ fontSize: '1.05rem' }}><i style={{ color: 'red' }}>Mỗi mã code bạn chỉ sử dụng được 1 lần.</i></span>
                </div>
                <div style={{ padding: '3rem 0' }}>
                    <img style={{ width: '100%' }} src="https://t3.ftcdn.net/jpg/04/65/46/52/240_F_465465254_1pN9MGrA831idD6zIBL7q8rnZZpUCQTy.jpg" alt="" />
                </div>
            </div>
        </div>
    );
}

export default DetailEvent;