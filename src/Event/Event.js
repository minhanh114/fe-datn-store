import React, { useEffect, useState } from 'react';
import './Event.css'
import queryString from 'query-string'
import CouponAPI from '../API/CouponAPI';
import Pagination from '../Shop/Component/Pagination';
import { Link } from 'react-router-dom';

function Event(props) {

    const [pagination, setPagination] = useState({
        page: '1',
        limit: '6',
        search: '',
        userId: sessionStorage.getItem('id_user')
    })
    const [coupons, setCoupons] = useState([])
    const [totalPage, setTotalPage] = useState()

    useEffect(() => {
        const query = '?' + queryString.stringify(pagination)

        const fetchAllData = async () => {
            const response = await CouponAPI.getCoupons(query)
            setCoupons(response.coupons)
            setTotalPage(response.totalPage)
        }
        fetchAllData()
    }, [pagination])


    const handlerChangePage = (value) => {
        setPagination({
            ...pagination,
            page: value
        })
    }

    return (
        <div>
            <div className="breadcrumb-area">
                <div className="container">
                    <div className="breadcrumb-content">
                        <ul>
                            <li><a href="index.html">Trang chủ</a></li>
                            <li className="active">Sự kiện</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="container" style={{ marginTop: '3rem' }}>
                {sessionStorage.getItem('id_user') ? (
                    <div>
                        <div className="grid-container" style={{ padding: '2rem' }}>
                            {
                                coupons && coupons.map(value => (
                                    <div className="bg_event animate__animated animate__zoomIn col_product" style={{ height: "auto" }}>
                                        <div>
                                            <div>
                                                <img style={{ width: 'auto', height: "200px" }} src="https://cdn.tgdd.vn/hoi-dap/1321785/banner-la-gi-khac-gi-voi-poster-cach-de-thiet-ke-mot%20(2).jpg" alt="" />
                                            </div>
                                            <div style={{ padding: '1rem 1.2rem' }}>
                                                <div style={{ height: "20px" }}>
                                                    <h6 className="h4_event">{"Khuyến mãi " + value.promotion + "%"}</h6>
                                                </div>

                                                <hr />
                                                <div style={{
                                                    marginTop: '.5rem', whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"
                                                }}>
                                                    <p style={{ fontSize: '1rem', color: 'gray' }} title={value.describe}>
                                                        {value.describe}
                                                    </p>
                                                </div>
                                                <div style={{ marginTop: '1rem' }} className="d-flex justify-content-center">
                                                    <Link to={`/event/${value._id}`} className="a_promotion">Xem Ngay</Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                        <div className="d-flex justify-content-center" style={{ padding: '2rem 0' }}>
                            <Pagination pagination={pagination} handlerChangePage={handlerChangePage} totalPage={totalPage} />
                        </div>
                    </div>
                ) : (
                    // Render alternative content if 'id_user' doesn't exist in the session
                    <div>
                        <p style={{ fontSize: '1.5rem', textAlign: 'center' }}>Vui lòng đăng nhập mới có thể xem các sự kiện đang diễn ra !</p>
                    </div>
                )}
            </div>
        </div>
    );
}

export default Event;