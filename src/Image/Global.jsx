import Logo from './shopping-bag.png'
import Footer1 from './2.png'
import Footer2 from './3.png'
import Footer3 from './4.png'
import Footer4 from './5.png'
import Footer5 from './pngtree-shopping-cart-icon-simple-png-image_2028930.jpg'
import Banner from './banner.jpg'
import Avatar from './avt.jpg'

const Image = {
    Logo,
    Footer1,
    Footer2,
    Footer3,
    Footer4,
    Banner,
    Avatar,
    Footer5
}

export default Image