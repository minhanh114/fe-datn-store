/* eslint-disable indent */
self.addEventListener("install", function (event) {
    event.waitUntil(
        caches.open("my-cache").then((cache) => {
            return cache.addAll(["/", "/index.html", "/manifest.json", "/icon.png"]);
        }
        ));
});

self.addEventListener("fetch", function (event) {
    event.respondWith(
        caches.match(event.request).then((response) => {
            return response || fetch(event.request);
        })
    );
});